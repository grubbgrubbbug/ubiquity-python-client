# Coin


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**handle** | **str** | Platform handle (platform var in path) | [optional] 
**slip44** | **int** | SatoshiLabs 0044, registered coin types: https://github.com/satoshilabs/slips/blob/master/slip-0044.md | [optional] 
**symbol** | **str** | Symbol of native currency | [optional] 
**name** | **str** | Name of platform | [optional] 
**block_time** | **int** | Average time between blocks (milliseconds) | [optional] 
**sample_address** | **str** | Random address seen on chain (optional) | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


