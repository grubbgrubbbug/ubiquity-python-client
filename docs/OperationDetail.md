# OperationDetail


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_from** | **str** | Sender address | [optional] 
**to** | **str** | Receiver address | [optional] 
**currency** | [**Currency**](Currency.md) |  | [optional] 
**value** | **str** | Integer string in smallest unit (Satoshis) | [optional] 
**inputs** | [**[Utxo]**](Utxo.md) |  | [optional] 
**outputs** | [**[Utxo]**](Utxo.md) |  | [optional] 
**unspent** | **str** | Integer string in smallest unit (Satoshis) | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


